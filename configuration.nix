{ config, lib, pkgs, ... }:
{
  imports =
    [
      ./hardware-configuration.nix
      <home-manager/nixos>
    ];

  # BOOT LOADER
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # NETWORKING
  networking.hostName = "river";
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    earlySetup = true;
    keyMap = "colemak";
    font = "${pkgs.terminus_font}/share/consolefonts/ter-132n.psf.gz";
    packages = with pkgs; [ terminus_font ];
  };

  sound.enable = true; 
  hardware.pulseaudio.enable = true;
  nixpkgs.config.pulseaudio = true;
  hardware.pulseaudio.extraConfig = "load-module module-combine-sink";


  users.users.lucas = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "audio" "video" ];
  };

  fonts.packages = with pkgs; [
     font-awesome 
  ];

  # home-manager.useGlobalPkgs = true;
  home-manager.users.lucas = { pkgs, ... }: {
    home.pointerCursor = {
      gtk.enable = true;
      # x11.enable = true;
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Classic";
      size = 16;
    };

    gtk = {
      enable = true;
      theme = {
        package = pkgs.flat-remix-gtk;
        name = "Flat-Remix-GTK-Grey-Darkest";
      };

      iconTheme = {
        package = pkgs.gnome.adwaita-icon-theme;
        name = "Adwaita";
      };

      font = {
        name = "Sans";
        size = 11;
      };
    };
    
    home.packages = [ 
      pkgs.brave
    ];

    programs.bash = {
      enable = true; 
      initExtra = ''
        PS1='\[\e[3m\]\h\[\e[0m\] \[\e[1m\][\[\e[0;38;5;38m\]\W\[\e[0;1m\]]\[\e[0m\] > '
      '';
      shellAliases = {
        config = "hx /etc/nixos/configuration.nix";
        update = "sudo nixos-rebuild switch --upgrade";
        clean = "sudo nix-collect-garbage -d";
      }; 
    };

    programs.foot = {

      enable = true;
      settings = {

        main = {
          font = "monospace:size=18";          
          pad = "10x10";
        };

        cursor = {
          style = "beam"; 
          blink = "yes";
        };

        colors = {
          foreground = "e5e5e5"; 
          background = "000000";
          regular0 = "000000";
          regular1 = "cd0000";
          regular2 = "00cd00";
          regular3 = "cdcd00";
          regular4 = "0000ee";
          regular5 = "cd00cd";
          regular6 = "00cdcd";
          regular7 = "e5e5e5";
          bright0 = "7f7f7f";
          bright1 = "ff0000";
          bright2 = "00ff00";
          bright3 = "ffff00";
          bright4 = "5c5cff";
          bright5 = "ff00ff";
          bright6 = "00ffff";
          bright7 = "ffffff";
        };

        key-bindings = {
          clipboard-copy = "Control+Shift+c XF86Copy";
          clipboard-paste = "Control+Shift+v XF86Paste";
        };
      };
    };

    programs.helix = {
      enable = true;
      defaultEditor = true; 
      settings = {
        theme = "curzon";
        editor.cursor-shape = {
          insert = "bar";
          normal = "block";
          select = "block";
        };
        editor.file-picker = {
          hidden = false; 
        };
      };
    };

    wayland.windowManager.hyprland = {
      enable = true; 
      systemd.variables = ["--all"];
      settings = {
        "$mod" = "SUPER"; 
        "$terminal" = "footclient";
        exec-once = [
          "foot -s"
          "waybar"
        ];
        bind = [
          "$mod, W, exec, brave"  

          "$mod, Return, exec, footclient"

          "$mod, M, exit"
          "$mod, Q, killactive"
          "$mod, V, togglefloating"

          "$mod, k, movefocus, l"
          "$mod, j, movefocus, r"

          "$mod, h, resizeactive, -40 0"
          "$mod, l, resizeactive, 40 0"
        ]
        ++ (
        # workspaces
        # binds $mod + [shift +] {1..10} to [move to] workspace {1..10}
        builtins.concatLists (builtins.genList (
            x: let
              ws = let
                c = (x + 1) / 10;
              in
                builtins.toString (x + 1 - (c * 10));
            in [
              "$mod, ${ws}, workspace, ${toString (x + 1)}"
              "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
            ]
          )
          10)
        );

        bindm = [
          "$mod, mouse:272, movewindow"
          "$mod, mouse:273, resizewindow"
        ];


        env = ["XCURSOR_SIZE,24" "HYPRCURSOR_SIZE,24" "XDG_CURRENT_DESKTOP,Hyprland"];

        monitor = "eDP-1,1920x1080@60,0x0,1";

        general = {
          gaps_in = 6;
          gaps_out = 6; 

          border_size = 2;

          resize_on_border = true;

          layout = "dwindle";
        };

        decoration = {
          rounding = 0;

          drop_shadow = false;
          shadow_range = 0;

          blur = {
            enabled = false; 
          }; 
        };

        animations = {
          enabled = false; 
        };

        dwindle = {
          pseudotile = true;
          preserve_split = true; 
        };

        misc = {
          force_default_wallpaper = 0; 
          disable_hyprland_logo = true;
        };

        input = {
          kb_layout = "us"; 
          kb_variant = "colemak";
          repeat_delay = 300;
          repeat_rate = 50;
        };
      };
    };

    programs.waybar = {
      enable = true;
      settings = {
        mainBar = {
          layer = "top";
          position = "bottom";
          height = 30;
          spacing = 4;

          modules-left = [ "hyprland/workspaces" ];
          modules-center = [ "hyprland/window" ];
          modules-right = [ "pulseaudio" "network" "backlight" "battery" "clock" ];

          "clock" = {
            tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
            format = "{:%I:%M %p}";
          };

          "backlight" = {
            format = "{percent}%  ";
          };

          "battery" = {
            states = {
              good = 95;
              warning = 30;
              critical = 15;
            };
            format = "{capacity}%  {icon}";
            format-full = "{capacity}%  {icon}";
            format-charging = "{capacity}%  ";
            format-plugged = "{capacity}%  ";
            format-alt = "{time}  {icon}";
            format-icons = ["" "" "" "" ""];
          };

          "network" = {
            format-wifi = "{essid} ({signalStrength}%)  ";
            format-ethernet = "{ipaddr}/{cidr} ";
            tooltip-format = "{ifname} via {gwaddr} ";
            format-linked = "{ifname} (No IP) ";
            format-disconnected = "Disconnected ⚠";
            format-alt = "{ifname}: {ipaddr}/{cidr}";
            on-click = "nmtui";
          };

          "pulseaudio" = {
            format = "{volume}%  {icon} {format_source}";
            format-bluetooth = "{volume}% {icon} {format_source}";
            format-bluetooth-muted = " {icon} {format_source}";
            format-muted = " {format_source}";
            format-source = "{volume}%  ";
            format-source-muted = "";
            on-click = "pavucontrol";
          };
        };
      };
      style = ''
        * {
          font-family: FontAwesome, Roboto, Helvetica, Arial, sans-serif;
          font-size: 16px;
        }

        window#waybar {
          background-color: rgba(00, 00, 00, 0.5);
          color: #ffffff;
        }

        button {
          box-shadow: inset 0 -3px transparent;
          border: none;
          border-radius: 0;
        }

        #workspaces button {
          padding: 0 5px;
          background-color: transparent;
          color: #ffffff;
        }

        #workspaces button:hover {
          background: rgba(0, 0, 0, 0.2);
        }

        #workspaces button.focused {
          background-color: #64727D;
          box-shadow: inset 0 -3px #ffffff;
        }

        #workspaces button.urgent {
          background-color: #eb4d4b;
        }

        #clock,
        #battery,
        #backlight,
        #network,
        #pulseaudio {
            padding: 0 10px;
            color: #ffffff;
            background-color: #000000;
        }

        #window,
        #workspaces {
            margin: 0 4px;
        }

        .modules-left > widget:first-child > #workspaces {
            margin-left: 0;
        }

        .modules-right > widget:last-child > #workspaces {
            margin-right: 0;
        }

        #battery.charging, #battery.plugged {
            color: #ffffff;
            background-color: #26A65B;
        }

        @keyframes blink {
        to {
            background-color: #ffffff;
            color: #000000;
          }
        }

      #battery.critical:not(.charging) {
          background-color: #f53c3c;
          color: #ffffff;
          animation-name: blink;
          animation-duration: 0.5s;
          animation-timing-function: steps(12);
          animation-iteration-count: infinite;
          animation-direction: alternate;
      }

      label:focus {
          background-color: #000000;
      }
        
      '';
    };

    # The state version is required and should stay at the version you
    # originally installed.
    home.stateVersion = "23.11";
  };

  environment.systemPackages = with pkgs; [
    git
    brightnessctl
    rustc
    cargo
    pavucontrol
  ];

  environment.defaultPackages = [];

  powerManagement.enable = true;
  services.tlp.enable = true;

  programs.nano.enable = false;
  # programs.river.enable = true;
  # programs.waybar.enable = true;
  programs.hyprland.enable = true;

  system.stateVersion = "23.11";

}

